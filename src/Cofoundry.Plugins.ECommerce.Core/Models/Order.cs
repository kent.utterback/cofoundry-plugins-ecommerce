﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cofoundry.Plugins.ECommerce.Core
{
    public class OrderItem : InventoryItem
    {

    }
    public class Order
    {
        public long Id { get; set; }
        public List<OrderItem> Items { get; set; }
        public float Subtotal { get; set; }
        public float Tax { get; set; }
        public float Total { get; set; }
    }
}
