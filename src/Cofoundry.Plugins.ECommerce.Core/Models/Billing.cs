﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cofoundry.Plugins.ECommerce.Core
{
    public class Billing : Address
    {
        public long Id { get; set; }
        public CreditCard CC { get; set; }
    }
    public class CreditCard
    {
        public long Id { get; set; }
        public long Number { get; set; }
        public DateTime Expiration { get; set; }
        public short CVV { get; set; }
    }
}
