﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cofoundry.Plugins.ECommerce.Core
{
    public class InvoiceItem : InventoryItem
    {

    }
    public class Invoice
    {
        public long Id { get; set; }
        public Term Term { get; set; }
        public InvoiceStatus Status { get; set; }
        public DateTime Issued { get; set; }
        public DateTime Due { get; set; }
        public List<InvoiceItem> Items { get; set; }
        public float LateFees { get; set; }
        public float Subtotal { get; set; }
        public float Tax { get; set; }
        public float Total { get; set; }
    }
    public enum InvoiceStatus
    {
        OnTime,
        OverdueTerm1,
        OverdueTerm2,
        OverdueTerm3,
        OverdueTerm4,
        OverdueTerm5,
        OverdueTerm6,
        Delinquent = 10
    }
    public class Term
    {


    }
    public enum TermType
    {
        Net10 = 0,
        Net15 = 1,
        Net30 = 3,
        Net60 = 6,
        Net90 = 9,
        Custom = 10
    }
}
