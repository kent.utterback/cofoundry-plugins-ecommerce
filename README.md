# Cofoundry Plugins E-Commerce

This plugin is an E-Commerce integration designed to work with the CMS [Cofoundry](https://www.cofoundry.org/). Cofoundry is already a capable CMS framework for .Net core applications. I would ideally like to see it compete with Wordpress as a good C# .Net-esk alternative. More plugins are needed and greater popularization hence the thought behind this plugin.

## Features

Applicable to many business models - Designed for a multitude of businesses whether you sell physical or digital goods.
Analytics and reporting - Built with data analysis, metrics, and exportable reports in mind makes this plugin easier to run your online business store.
Dependancy Injection Model - Taking a page from the rest of the Cofoundry design this plugin is designed to be implemented via depeancy injection keeping the original Cofoundry untouched.

## Roadmap 

See the [Gitlab Issues](https://gitlab.com/kent.utterback/cofoundry-plugins-ecommerce/issues) for milestones and features to come.
