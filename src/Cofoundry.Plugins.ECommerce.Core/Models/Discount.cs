﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cofoundry.Plugins.ECommerce.Core
{
    class Discount
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<InventoryItem> Items { get; set; }
        public Type Type { get; set; }
        public Status Status { get; set; }
        public float Price { get; set; }
        public int Percentage { get; set; }
        public DateTime ValidStartDate { get; set; }
        public DateTime ValidEndDate { get; set; }
        public DateTime SecondaryValidStartDate { get; set; }
        public DateTime SecondaryValidEndDate { get; set; }
        public Recurrence Recurrence { get; set; }

    }

    public class Recurrence
    {
        public bool Reoccur { get; set; }
        public When When { get; set; }
    }
    public enum When
    {
        Daily,
        Biweekly,
        Weekly,
        Bimonthly,
        Monthly,
        Yearly
    }
}
