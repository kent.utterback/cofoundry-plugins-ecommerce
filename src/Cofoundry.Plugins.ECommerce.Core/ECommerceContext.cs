﻿using System;
using Cofoundry.Core.EntityFramework;
using Cofoundry.Domain.Data;
using Microsoft.EntityFrameworkCore;

namespace Cofoundry.Plugins.ECommerce.Core
{
    public class ECommerceContext : CofoundryDbContext
    {
        public ECommerceContext(ICofoundryDbContextInitializer cofoundryDbContextInitializer): base(cofoundryDbContextInitializer){}

        public DbSet<Customer> Customers { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    modelBuilder
        //        .HasAppSchema()
        //        .ApplyConfiguration(new CustomerMap());
        //}
    }
}
