﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cofoundry.Plugins.ECommerce.Core
{
    public class CartItem : InventoryItem
    {

    }
    public class Cart
    {
        public long Id { get; set; }
        public DateTime Expiration { get; set; }
        public List<CartItem> Items { get; set; }
        public float Subtotal { get; set; }
        public float Tax { get; set; }
        public float Total { get; set; }
    }
}
