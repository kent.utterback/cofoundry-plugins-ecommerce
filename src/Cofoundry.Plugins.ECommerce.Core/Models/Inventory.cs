﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cofoundry.Plugins.ECommerce.Core
{
    public class InventoryItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Type Type { get; set; }
        public Status Status { get; set; }
        public float Price { get; set; }
        public int Quantity { get; set; }

    }
    public enum Type
    {
        Stock,
        Subscription,
        Digital
    }
    public enum Status
    {
        Available,
        BackOrder,
        Unavailable
    }
    public class Inventory
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<InventoryItem> Items { get; set; }
    }

    public class Collection
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<Inventory> Collections { get; set; }
    }
}
