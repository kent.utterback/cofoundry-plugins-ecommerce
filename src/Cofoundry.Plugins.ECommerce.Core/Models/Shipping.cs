﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cofoundry.Plugins.ECommerce.Core
{
    public class Shipping
    {
        public long Id { get; set; }
        public Service Service { get; set; }
        public DateTime Date { get; set; }
        public Address Address { get; set; }

    }
    public class Service
    {
        public ServiceName Name { get; set; }
        public string Tracking { get; set; }
        public float Charge { get; set; }
        public string Status { get; set; }
    }
    public enum ServiceName
    {
        USPS,
        FedEx,
        UPS
    }
}
